#!/usr/bin/env python
import os, sys

django_libs = '../libs'
if os.path.exists(django_libs):
    for lib in os.listdir(django_libs):
        path = os.path.abspath(os.path.join(django_libs, lib))
        sys.path.insert(1,path)

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "{{project_name}}.settings")
    os.environ.setdefault("DJANGO_CONFIGURATION", "Settings")
    from cadocms.management import execute_from_command_line
    execute_from_command_line(sys.argv)
