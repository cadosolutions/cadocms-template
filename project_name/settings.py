from cadocms.settings import HostSettings, Settings as BaseSettings
from django.conf.urls import url

class Settings(BaseSettings):
    CADO_NAME = 'Example Project'
    CADO_CODE = '{{project_name}}'
    SECRET_KEY = '{{secret_key}}'
    
    @property
    def EXTRA_URLS(self):
        return [
                url(r'^$', '{{project_name}}.views.my_index_view'),
                url(r'^item/(\d+)$', '{{project_name}}.views.my_item_view'),
                #if nothing matches check if this is staticpage:
                url(r'^(?P<url>.*)$', 'cadocms.views.staticpage'),
                ] + super(Settings, self).EXTRA_URLS
    
"""
class ProdHostSettings(HostSettings):
    CLASS = 'PROD'
    DOMAIN = 'yourproductiondomain.com'
    NAME = 'yourserverfullhostname'
    SRCROOT = 'absolute location to sources on server'
    DATABASE = {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'yourdbname',
        'USER': 'yourdbuser',
        'PASSWORD': 'yourdbpass',
    }
"""
