from django.db import models
from cadocms.models import StaticPage as StaticPageBase
from cadocms.models import Chunk as ChunkBase

# CadoCMS defines StaticPage and Chunk as abstract model
# you can inherit those and extend for your needs
class StaticPage(StaticPageBase):
    pass

class Chunk(ChunkBase):
    pass

# example model
class MyModel(models.Model):
    some_int = models.IntegerField()
    some_text = models.TextField()
    
    @models.permalink
    def get_absolute_url(self):
        return ('{{project_name}}.views.my_item_view', [str(self.id)])
