# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'StaticPage'
        db.create_table(u'{{project_name}}_staticpage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('url', self.gf('django.db.models.fields.CharField')(max_length=200, db_index=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('title_en', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('content', self.gf('cadocms.fields.HTMLField')()),
            ('content_en', self.gf('cadocms.fields.HTMLField')(null=True, blank=True)),
            ('seo_title', self.gf('django.db.models.fields.CharField')(max_length=512, blank=True)),
            ('seo_title_en', self.gf('django.db.models.fields.CharField')(max_length=512, null=True, blank=True)),
            ('seo_keywords', self.gf('django.db.models.fields.CharField')(max_length=512, blank=True)),
            ('seo_keywords_en', self.gf('django.db.models.fields.CharField')(max_length=512, null=True, blank=True)),
            ('seo_description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('seo_description_en', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'{{project_name}}', ['StaticPage'])

        # Adding model 'Chunk'
        db.create_table(u'{{project_name}}_chunk', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('key', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('body', self.gf('cadocms.fields.HTMLField')(null=True, blank=True)),
            ('body_en', self.gf('cadocms.fields.HTMLField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'{{project_name}}', ['Chunk'])

        # Adding model 'MyModel'
        db.create_table(u'{{project_name}}_mymodel', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('some_int', self.gf('django.db.models.fields.IntegerField')()),
            ('some_text', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'{{project_name}}', ['MyModel'])


    def backwards(self, orm):
        # Deleting model 'StaticPage'
        db.delete_table(u'{{project_name}}_staticpage')

        # Deleting model 'Chunk'
        db.delete_table(u'{{project_name}}_chunk')

        # Deleting model 'MyModel'
        db.delete_table(u'{{project_name}}_mymodel')


    models = {
        u'{{project_name}}.chunk': {
            'Meta': {'object_name': 'Chunk'},
            'body': ('cadocms.fields.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'body_en': ('cadocms.fields.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        u'{{project_name}}.mymodel': {
            'Meta': {'object_name': 'MyModel'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'some_int': ('django.db.models.fields.IntegerField', [], {}),
            'some_text': ('django.db.models.fields.TextField', [], {})
        },
        u'{{project_name}}.staticpage': {
            'Meta': {'ordering': "('url',)", 'object_name': 'StaticPage'},
            'content': ('cadocms.fields.HTMLField', [], {}),
            'content_en': ('cadocms.fields.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'seo_description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'seo_description_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'seo_keywords': ('django.db.models.fields.CharField', [], {'max_length': '512', 'blank': 'True'}),
            'seo_keywords_en': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True', 'blank': 'True'}),
            'seo_title': ('django.db.models.fields.CharField', [], {'max_length': '512', 'blank': 'True'}),
            'seo_title_en': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '200', 'db_index': 'True'})
        }
    }

    complete_apps = ['{{project_name}}']