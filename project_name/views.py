from django.template import RequestContext
from django.shortcuts import get_object_or_404, redirect, render, render_to_response
from {{project_name}} import models

def my_index_view(request):
    context = {};
    context['myobjects'] = models.MyModel.objects.all();
    return render_to_response('index.html', context, context_instance=RequestContext(request))

def my_item_view(request, id):
    context = {};
    context['myobject'] = models.MyModel.objects.get(id=id);
    return render_to_response('item.html', context, context_instance=RequestContext(request))