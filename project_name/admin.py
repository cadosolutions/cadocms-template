from django.contrib import admin
from cadocms.admin import StaticPageAdmin, Setting, SettingAdmin, ChunkAdmin
from {{project_name}} import models


admin.site.register(models.MyModel)
admin.site.register(models.StaticPage, StaticPageAdmin)
admin.site.register(models.Chunk, ChunkAdmin)
admin.site.register(Setting, SettingAdmin)
