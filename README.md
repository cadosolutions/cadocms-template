cadocms-example
==========
Usage
----------

    :::bash
    # create new django project using this template
    django-admin.py startproject --extension=py,json --template=https://bitbucket.org/cadosolutions/cadocms-template/get/master.zip myproject
    cd myproject
    # if you want to use virtualenv (opitonal but recommended):
    virtualenv data/virtualenv
    source data/virtualenv/bin/activate
    # install requirements
    pip install -r requirements.txt
    # create data folders, create database and fill it with initial data
    ./manage.py install
    # run development server
    ./manage.py runserver
    # now navigate your browser to http://localhost:8000/ and enjoy

Notes
----------

Each host should have its config class (example one is comented out in settings.py)
There is one global class for "DEV" host available (built into cms)
it is using sqlitedb, dummy search engines etc. so you are able to set up development environment as quickly as possible.

In case of auth table warrning on install run sqls manually:

    :::bash
	python manage.py sql auth
    python manage.py sql admin


